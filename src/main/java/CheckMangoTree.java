import java.util.*;

public class CheckMangoTree {

    public static boolean isMangoTree(int rows, int cols, int tree_num){
        if(tree_num<cols && tree_num>=0)
            return true;
        else{
            return tree_num % cols - 1 == 0 || tree_num % cols == 0;
        }
    }

    public static void main(String[] args){
        Scanner sc= new Scanner(System.in);
        int rows= sc.nextInt();
        int cols= sc.nextInt();
        int tree_num= sc.nextInt();
        if(isMangoTree(rows, cols, tree_num)){
            System.out.println("yes");
        }
        else{
            System.out.println("no");
        }
    }
}